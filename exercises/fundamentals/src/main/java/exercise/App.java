package exercise;

class App {
    public static void numbers() {
        // BEGIN
        int expr1 = 8 / 2;
        int expr2 = 100 % 3;

        System.out.println(expr1 + expr2);
        // END
    }

    public static void strings() {
        String language = "Java ";
        // BEGIN
        String works = "works on JVM";
        System.out.println(language + works);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = " spartans";
        // BEGIN
        System.out.println(soldiersCount + name);

        // END
    }
}
